extends Control

var display = 30
#время в секундах до появления кнопки закрытия меню
onready var timer = get_node("Timer")
	
func _on_Margin_visibility_changed():
	$CanvasLayer/Margin/ColorRect/Close_button.set_visible(false)
	$CanvasLayer/Margin/ColorRect/time_display.set_visible(true)
	if $CanvasLayer/Margin.visible == true:
		timer.start()
		
func _on_Timer_timeout():
	display -= 1
	get_node("CanvasLayer/Margin/ColorRect/time_display").text = str(display)
	if display == 0:
		timer.stop()
		$CanvasLayer/Margin/ColorRect/Close_button.set_visible(true)
		display = 30
		$CanvasLayer/Margin/ColorRect/time_display.set_visible(false)
func _on_Close_button_pressed():
	$CanvasLayer/Margin.set_visible(false)
	#закрывает окно меню
	
func _drag_rect(event):
	if event is InputEventMouseMotion and Input.is_mouse_button_pressed(BUTTON_LEFT):
		$CanvasLayer/Margin.rect_position += event.relative
	#перемещение меню 
