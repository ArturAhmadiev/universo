extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "rajtigo"


# URL к API
const URL_DATA = "https://t34.tehnokom.su/api/v1.1/"

# количество объектов для загрузки
const count_objekto = "10"


# запрос на список управляемых объектов для вебсокета
func get_direktebla_json(statusoId, tipoId, id=0):
	if !id:
		id = Net.get_current_query_id()
	var statusoIdIn = String(Net.statuso_nova) + ', '+String(Net.statuso_laboranta) + ', '+String(Net.status_pauzo)
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload': {"query": "query ($UzantoId:Int, $statusoId:Float, "+
		" $tipoId:Float, $statusoIdIn:String)"+
		"{ universoObjekto (" +
		" universoobjektouzanto_Isnull:false,"+
		" universoobjektouzanto_Autoro_SiriusoUzanto_Id:$UzantoId," +
		") { edges { node { uuid integreco" +
		" projekto (statuso_Id: $statusoId, tipo_Id: $tipoId){ "+
		"  edges { node { uuid "+
		"  kategorio {edges {node {objId nomo{enhavo}}}}" +
		"  tasko (statuso_Id_In:$statusoIdIn){ edges {node { "+
		"   uuid finKoordinatoX finKoordinatoY finKoordinatoZ "+
		"   objekto{uuid nomo {enhavo}} " +
		"   kategorio {edges {node {objId nomo{enhavo}}}}" +
		"   pozicio statuso {objId } } } } } } } "+
		" nomo { enhavo } priskribo { enhavo } "+
		" resurso { objId nomo { enhavo } priskribo { enhavo } "+
		"  tipo { objId nomo { enhavo } } "+
		" } "+
		" koordinatoX koordinatoY koordinatoZ "+
		' posedantoObjekto '+
		'  { uuid } '+
		" stato{objId potenco statoAntaua {integreco} "+
		"    statoSekva{integreco}} integreco" +
		" ligiloLigilo{edges{node{uuid "+
		"  posedanto{ kubo {objId} koordinatoX koordinatoY koordinatoZ }}}}" +
		" ligilo{edges{node{ "+
		"  konektiloPosedanto konektiloLigilo tipo{objId}"+
		"  ligilo{ uuid integreco nomo{enhavo} integreco resurso{objId} "+
		"   stato{objId potenco statoAntaua {integreco} "+
		"    statoSekva{integreco}}" +
		"   ligilo{edges{node{" +
		"    konektiloPosedanto konektiloLigilo tipo{objId} " +
		"    ligilo{ uuid integreco resurso{objId} }}}}}" +
		"    tipo{objId}}}}" +
		" realeco{objId}" +
		" posedanto{edges{node{" +
		"  posedantoUzanto{ siriusoUzanto{ objId}}}}}" +
		" rotaciaX rotaciaY rotaciaZ } } } }",
		'variables': {"statusoId":statusoId, 
		"tipoId":tipoId, "statusoIdIn":statusoIdIn,
		"UzantoId":Global.id} } })

	# print("=== get_direktebla_query = ",query)
	return query
	

# запрос на список управляемых объектов в космосе
func get_direktebla_kosmo_json(id=0):
	if !id:
		id = Net.get_current_query_id()
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload': { "query": "query ($UzantoId:Int )"+
	"{ filteredUniversoObjekto (" +
	" universoobjektouzanto_Isnull:false,"+
	" universoobjektouzanto_Autoro_SiriusoUzanto_Id:$UzantoId," +
	" koordinatoX_Isnull:false, koordinatoY_Isnull:false, koordinatoZ_Isnull:false," +
	" kubo_Isnull:false," +
	") { edges { node { uuid " +
	"  realeco{objId}}}}}",
	'variables': {"UzantoId":Global.id} } })
	# print("=== get_direktebla_kosmo_query = ",query)
	return query


# задаём координаты и угол поворота объекту, удаляем связь по нахождению внутри
func go_objekt_kosmo_query(uuid, koordX, koordY, koordZ, rotaciaX, rotaciaY, rotaciaZ, uuid_ligilo_del, kuboId=Global.kubo):
	var del = ''
	if uuid_ligilo_del:
		del = ' redaktuUniversoObjektoLigiloj (uuid: "'+uuid_ligilo_del+'",'
		del = del + 'forigo:true) {status message universoObjektojLigiloj{ uuid } } '

	var query = JSON.print({ 'query': 'mutation ($uuid:UUID, $koordX:Float, $koordY:Float, $koordZ:Float,'+
		' $rotaciaX:Float, $rotaciaY:Float, $rotaciaZ:Float, $kuboId:Int )'+
		' { redaktuUniversoObjekto ( uuid: $uuid, koordinatoX: $koordX, koordinatoY: $koordY, '+
		' koordinatoZ: $koordZ, rotaciaX: $rotaciaX, rotaciaY: $rotaciaY, rotaciaZ: $rotaciaZ, '+
		' kuboId: $kuboId) '+
		' { status message universoObjektoj { uuid } } ' + del + '}',
		'variables': {"uuid":uuid, "koordX": koordX, "koordY": koordY, "koordZ": koordZ,
		"rotaciaX": rotaciaX, "rotaciaY": rotaciaY, "rotaciaZ": rotaciaZ, 
		"kuboId":kuboId } })
	# print('==go_objekt_kosmo_query == ', query)
	return query


# завершение задачи, проекта, запись в таблицу связей, что находимся внутри другого объекта
# station_uuid - в какой станции находимся
func eniri_kosmostacio(projekto_uuid, tasko_uuid, station_uuid):
	var statusoId = 4
	var tipoId = 3 #находится внутри
	var param = ''
	var mutateProjekto = ''

	if projekto_uuid:
		param = ', $tasko_uuid:UUID, $projekto_uuid:UUID, $statusoId:Int'
		mutateProjekto = 'redaktuUniversoTaskoj (uuid: $tasko_uuid, statusoId:$statusoId) { status '
		mutateProjekto = mutateProjekto + ' message universoTaskoj { uuid } } '
		mutateProjekto = mutateProjekto + 'redaktuUniversoProjekto (uuid: $projekto_uuid,  '
		mutateProjekto = mutateProjekto + ' statusoId:$statusoId) { status '
		mutateProjekto = mutateProjekto + ' message universoProjekto { uuid } } '
	return JSON.print({ 'query': 'mutation ($posedantoUuid:String, $ligiloUuid:String, '+
		' $tipoId:Int '+param+')'+
		'{ '+
		' redaktuUniversoObjektoLigiloj ( posedantoUuid:$posedantoUuid,' +
		'  ligiloUuid:$ligiloUuid, tipoId:$tipoId, publikigo:true) { message status' +
		'  universoObjektojLigiloj{ uuid } }' +
		mutateProjekto +
		'}',
		'variables': {"tasko_uuid":tasko_uuid, "statusoId": statusoId, "projekto_uuid":projekto_uuid,
			"posedantoUuid":station_uuid, 
			"ligiloUuid":Global.direktebla_objekto[Global.realeco-2]['uuid'], 
			"tipoId":tipoId } })


# Запрос к API, выбираем объекты, которые в космосе
# statusoId - статус проекта (2=в работе)
# tipoId - тип проекта Универсо (2 - Для объектов)
func get_objekto_json(statusoId, tipoId, kuboId=Global.kubo, id=0, after=""):
	if !id:
		id = Net.get_current_query_id()
	var statusoIdIn = String(Net.statuso_nova) + ', '+String(Net.statuso_laboranta) + ', '+String(Net.status_pauzo)
	var query = JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ "query": "query ($kuboId:Float, $statusoId:Float, "+
		" $realecoId:Float, $tipoId:Float, $statusoIdIn: String) " +
		"{ filteredUniversoObjekto (realeco_Id:$realecoId, kubo_Id: $kuboId, "+
		" koordinatoX_Isnull:false, koordinatoY_Isnull:false, koordinatoZ_Isnull:false," +
		" first:"+count_objekto+', after: "'+after+'" ' +
		" ) { pageInfo { hasNextPage endCursor } edges { node { uuid posedantoId "+
		" integreco" +
		# проект стрельбы  с задачами прицеливания и ведения огня

		# проект движени
		# " projekto (statuso_Id: $statusoId, tipo_Id: $tipoId, "+
		# "   kategorio_Id:$kategorioMovado){ "+
		" projekto (statuso_Id: $statusoId, tipo_Id: $tipoId){ "+
		"  edges { node { uuid "+
		"  kategorio {edges {node {objId nomo{enhavo}}}}" +
		"  tasko (statuso_Id_In: $statusoIdIn){ edges {node { "+
		"   uuid finKoordinatoX finKoordinatoY finKoordinatoZ "+
		"   objekto{uuid nomo {enhavo}} " +
		"   kategorio {edges {node {objId nomo{enhavo}}}}" +
		"   pozicio statuso {objId} } } } } } } "+
		" nomo { enhavo } priskribo { enhavo } "+
		" resurso { objId nomo { enhavo } priskribo { enhavo } "+
		"  tipo { objId nomo { enhavo } } "+
		" } "+
		" koordinatoX koordinatoY koordinatoZ "+
		' posedantoObjekto '+
		'  { uuid } '+
		" stato{objId potenco statoAntaua {integreco} "+
		"    statoSekva{integreco}} integreco" +
		" ligilo{edges{node{ "+
		"  konektiloPosedanto konektiloLigilo tipo{objId}"+
		"  ligilo{ uuid integreco nomo{enhavo} integreco resurso{objId} "+
		"   stato{objId potenco statoAntaua {integreco} "+
		"    statoSekva{integreco}}" +
		"   ligilo{edges{node{" +
		"    konektiloPosedanto konektiloLigilo tipo{objId} " +
		"    ligilo{ uuid integreco resurso{objId} }}}}}" +
		"    tipo{objId}}}}" +
		" posedanto{edges{node{" +
		"  posedantoUzanto{ siriusoUzanto{ objId}}}}}" +
		" rotaciaX rotaciaY rotaciaZ } } } }",
		'variables': {"kuboId":kuboId, "statusoId":statusoId, 
		"tipoId":tipoId, "statusoIdIn":statusoIdIn,
		"realecoId":Global.realeco} } })
	# print('===objecto_json=',query)
	return query



