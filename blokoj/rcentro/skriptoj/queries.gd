extends Object
# Здесь будем хранить всё для запросов к бэкэнду по блоку "Ресурсный центр"


# URL к API
const URL = "https://t34.tehnokom.su/api/v1.1/"


# Запрос к API
func resurso_query():
	return JSON.print({ "query": "{ resursoj { edges { node { uuid nomo { enhavo } } } } }" })
