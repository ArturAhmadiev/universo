extends KinematicBody

export var Sensitivity_X: float = 0.01
export var Sensitivity_Y: float = 0.01

const ZOOM_MIN = 1
const ZOOM_MAX = 50
const Zoom_Step: float = 1.0
#const MIN_ROT_Y = -1.55 #(89 градусов)
#const MAX_ROT_Y = 0.79 #(45 градусов)

var max_speed =500.0
# текущая скорость
var current_speed =0
var acceleration = 1
var way_point: Vector3 = Vector3() # Координаты точки, в которую летим
var target_dir: Vector3 = Vector3.ZERO #направление на эту точку от текущей позиции корабля
var target_rot = null #положение корабля, которое надо принять, чтобы нацелиться на точку.
var speed_rotation = 1
var middle_mouse_pressed = false
var docking_rotation
var uuid #uuid активного корабля игрока
# взятые в прицел, массив объектов, взятых в прицел
var celilo = []
var max_celilo = 1 # максимальное количество прицелов
var livero = false # произошел выстрел и запущена перезарядка

func _ready():
	$CollisionShape.queue_free()


func _physics_process(delta):
	# если категория задачи равна категории движения объекта, тогда двигаемся
	if !(Global.fenestro_itinero.itineroj.empty() or \
			Global.fenestro_itinero.itinero_pause) and \
			(Global.fenestro_itinero.itineroj.front()['kategorio']==Net.kategorio_movado): #Если цель существует, двигаемся
		if !target_rot:
#			print('target_rot = ',Global.fenestro_itinero.itineroj.front()['transform'].origin)
			target_rot = Quat(transform.looking_at(Global.fenestro_itinero.itineroj.front()['transform'].origin,Vector3.UP).basis) #запоминаем в какое положение надо установить корабль, чтобы нос был к цели. Это в кватернионах. ХЗ что это, но именно так вращать правильнее всего.

		var target_dir = (Global.fenestro_itinero.itineroj.front()['transform'].origin - translation).normalized()
		var distance = translation.distance_to(Global.fenestro_itinero.itineroj.front()['transform'].origin)
		if (len (Global.fenestro_itinero.itineroj)>1) and \
				(Global.fenestro_itinero.itineroj[1]['kategorio']==Net.kategorio_movado):
			# проверяем, если следующая точка является движением
			if distance <1: #Если это не последняя точка, то мы не тормозим, так что завышаем расстояние, чтобы не проскочить эту точку
				dec_route()
				return
			current_speed = lerp(current_speed,max_speed,delta*acceleration)#ускоряемся
			transform.basis = transform.basis.slerp(target_rot,speed_rotation*delta) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
		else:
			if distance > max_speed*delta/acceleration:#Тут сомнительная формула от фонаря, вычисляющая примерно откуда надо начинать тормозить корабль.
				current_speed = lerp(current_speed,max_speed,delta*acceleration)#ускоряемся
				var a = Quat(transform.basis)
				var c = a.slerp(target_rot,speed_rotation*delta) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
				transform.basis = Basis(c)
#				transform.basis = transform.basis.slerp(target_rot,speed_rotation*delta) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
			else: #Если это  последняя точка, то мы тормозим, и задаём минимальное расстояние, чтобы точнее выставить корабль
				current_speed = lerp(current_speed,50,delta*acceleration)#замедляемся
				if Global.fenestro_itinero.itineroj.front()['transform'].basis !=Basis.IDENTITY:
					transform.basis = transform.basis.slerp(Quat(Global.fenestro_itinero.itineroj.front()['transform'].basis),speed_rotation*delta*1.5) #поворачиваем в дефолтное состояние, чтобы сесть
				if distance <0.01:
					if Global.fenestro_itinero.itineroj.front()['transform'].basis !=Basis.IDENTITY:
						transform.basis = Global.fenestro_itinero.itineroj.front()['transform'].basis
					translation = Global.fenestro_itinero.itineroj.front()['transform'].origin
					clear_route()
					return
# warning-ignore:return_value_discarded
		move_and_slide(target_dir*delta*current_speed) #Двигаемся к цели  и тут можно передать transform на сервер
	Title.get_node("CanvasLayer/UI/Objektoj/Window").distance_to(translation)
	# вывод целей на экран
#	if get_node_or_null('Control'):
	if celilo.size()>0:
		if livero:
			$Control/kanonadi/livero.value = $Control/kanonadi/livero.value + 1
#		else:
#			$Control/celilo.set_visible(false)
#			$Control/celilo/label.text = ''
		


func set_celilo(celo):
	celilo.append(celo)
	$Control/celilo.set_visible(true)
	$Control/celilo/label.text = celilo.front().uuid
	get_node("laser_gun").set_target(celilo.front())


func rotate_start():# поворачиваем корабль носом по ходу движения
	var front = Transform(Basis.IDENTITY, Vector3(Global.fenestro_itinero.itineroj.front()['koordinatoX'],
		Global.fenestro_itinero.itineroj.front()['koordinatoY'], 
		Global.fenestro_itinero.itineroj.front()['koordinatoZ']))
	target_rot = Quat(transform.looking_at(front.origin,Vector3.UP).basis) #запоминаем в какое положение надо установить корабль, чтобы нос был к цели. Это в кватернионах. ХЗ что это, но именно так вращать правильнее всего.


func dec_route():
	target_rot = null
	Global.fenestro_itinero.malmultigi_unua()
	rotate_start()


func clear_route():
	target_rot = null
	# достигли цели, закрываем проект
	Global.fenestro_itinero.fermi_projekto_tasko()


const QueryObject = preload("../skriptoj/queries.gd")

func _on_timer_timeout():
	var q = QueryObject.new()
	# Делаем запрос к бэкэнду
	$"../http_mutate".request(q.URL, Global.backend_headers, true, 2, 
		q.objecto_mutation(uuid, translation.x, 
			translation.y, translation.z,
			rotation.x, 
			rotation.y, rotation.z)
	)


func _on_kanonadi_pressed():
	if celilo.size()>0:
		if $Control/kanonadi.pressed:
#			постановка цели
#			get_node("laser_gun").set_target(celilo.front())
			get_node("laser_gun").pafo = true
			livero = true
			# отправляем выстрел на сервер
			
		else:
			get_node("laser_gun").pafo = false
#			get_node("laser_gun").set_target('')
	else:
		$Control/kanonadi.pressed = false
		get_node("laser_gun").pafo = false


func _on_livero_value_changed(value):
	if value == 20:
		get_node("laser_gun").pafo = false
#		get_node("laser_gun").set_target('')
	elif value == 100:
		livero = false
		$Control/kanonadi/livero.value = 0
		_on_kanonadi_pressed()
