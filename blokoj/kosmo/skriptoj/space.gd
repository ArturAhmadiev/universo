extends Spatial


const QueryObject = preload("queries.gd")


# warning-ignore:unused_signal
signal load_objektoj

# список id запросов, отправленных на сервер
var id_projekto_direkt_del = [] # список проектов на удаление
var subscription_id # id сообщения, по какому объекту, в каком проекте, какая задача изменена
var id_net = [] # список пустых id запросов (ответы не анализируются, а удаляются)

# пока сервер не закрывает выстрел, закрывает сам клиент
var id_pafo = []


func _ready():
	$Control.camera = $camera

	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('error = ',err)

	Global.fenestro_kosmo = self
	# считываем размер экрана и задаём затемнение на весь экран
	$ui/loading.margin_right = get_node("/root").get_viewport().size.x
	$ui/loading.margin_bottom = get_node("/root").get_viewport().size.y
	# создаём свой корабль
	var ship = create_ship(Global.direktebla_objekto[Global.realeco-2])
	#если корабль игрока, то брать данные из direktebla_objekto

	$camera.translation=Vector3(
		Global.direktebla_objekto[Global.realeco-2]['koordinatoX'],
		Global.direktebla_objekto[Global.realeco-2]['koordinatoY'], 
		Global.direktebla_objekto[Global.realeco-2]['koordinatoZ']+22
	)
	add_child(ship,true)
	$camera.point_of_interest = ship

	for i in get_children():
		if has_signal_custom(i,"new_way_point"):
			i.connect("new_way_point",self,"set_way_point")
	
	subscribtion_kubo()


func _on_data():
	var i_data_server = 0
	var q = QueryObject.new()
	for on_data in Net.data_server:
		var index = id_projekto_direkt_del.find(int(on_data['id']))
		var index_net = id_net.find(int(on_data['id']))
		var index_pafo = id_pafo.find(int(on_data['id']))
		if index_net > -1: # находится в списке отправленных запросов
			# удаляем из списка
			id_net.remove(index_net)
			Net.data_server.remove(i_data_server)
		elif index > -1: # находится в списке удаляемых объектов
			# удаляем из списка
			var idx_prj = 0 #индекс массива для удаления
			for prj in Global.direktebla_objekto[Global.realeco-2]['projekto']['edges']:
				if prj['node']['uuid']==on_data['payload']['data']['redaktuUniversoProjekto']['universoProjekto']['uuid']:
					Global.direktebla_objekto[Global.realeco-2]['projekto']['edges'].remove(idx_prj)
				idx_prj += 1
			id_projekto_direkt_del.remove(index)
			Net.data_server.remove(i_data_server)
		elif index_pafo > -1: # находится в списке выстрелов, закрываем задачу
			# пользователь не может закрыть, т.к. владелец лазера объект, а не пользователь
			var id = Net.get_current_query_id()
			Global.fenestro_kosmo.id_net.push_back(id)
			Net.send_json(q.finado_tasko(on_data['payload']['data']['redaktuUniversoTaskoj']['universoTaskoj']['uuid'],Net.statuso_fermo,id))
			id_pafo.remove(index_pafo)
			Net.data_server.remove(i_data_server)
		elif int(on_data['id']) == subscription_id:
			if on_data['payload']['data']['universoObjektoEventoj']['evento']=='kubo_tasko':
				# пришло сообщение, по какому объекту, в каком проекте, какая задача изменена
				sxangxi_tasko(on_data['payload']['data'])
			elif on_data['payload']['data']['universoObjektoEventoj']['evento']=='kubo_objekto':
				# изменение по объекту
				sxangxi_objekto(on_data['payload']['data'])
			Net.data_server.remove(i_data_server)
		i_data_server += 1


# проверка, входит ли uuid в данный объект
#проверка по нисходящий (по всем детям-объектам)
func sercxado_uuid(uuid, objekt):
	var result = null
	if objekt.get('uuid'):
		if uuid == objekt.uuid:
			return objekt
	for ch in objekt.get_children():
		result = sercxado_uuid(uuid, ch)
		if result:
			return result
	return result


# поиск объекта по uuid
func sercxo_objekto_uuid(uuid):
	for ch in get_children():
		if ch.is_in_group('create'):
			if ch.uuid == uuid:
				return ch
	return null


# меняем задачу объекту
func sxangxi_tasko(on_data):
	# пометка о нахождении нужного объекта
	var objekto = false
	# если эти изменения не по задаче
	if not on_data['universoObjektoEventoj']['tasko']:
		return
	var uuid = on_data['universoObjektoEventoj']['tasko']['posedanto']['edges'].front()['node']['posedantoObjekto']['uuid']
	for ch in get_children(): # проходим по всем созданным объектам (кроме управляемого корабля) в поисках нужного
		if ch.is_in_group('create') and ('objekto' in ch):
			if sercxado_uuid(uuid,ch):
#			отправляю астероиду - ха-ха-ха
#			if on_data['universoObjektoEventoj']['objekto']['uuid']==ch.uuid:
				# если это задача категории движения
				if on_data['universoObjektoEventoj']['projekto']['kategorio']['edges'].front()['node']['objId']==Net.kategorio_movado:
					# изменение маршрута движения
					ch.sxangxi_itinero(on_data['universoObjektoEventoj']['projekto'],
						on_data['universoObjektoEventoj']['tasko'])
				# если проект стрельбы
				elif on_data['universoObjektoEventoj']['projekto']['kategorio']['edges'].front()['node']['objId']==Net.projekto_kategorio_pafado:
					ch.pafo(on_data['universoObjektoEventoj']['projekto'],
						on_data['universoObjektoEventoj']['tasko'])
					if on_data['universoObjektoEventoj']['tasko']['kategorio']['edges'].front()['node']['objId']==Net.tasko_kategorio_celilo:
						# сообщаем кораблю игрока о прицеливании в него
						pass
				objekto = true
	if !objekto: # если объект не найден, то нужно его добавить (если он не часть своего корабля)
		if sercxado_uuid(uuid,$ship):
			print('часть управляемого корабля')
		else:
			print('===найден новый объект!!! uuid=',uuid,' ===on_data=',on_data)


# изменяем характеристики объекта
func sxangxi_objekto(on_data):
	var uuid = on_data['universoObjektoEventoj']['objekto']['uuid']
	var objekto = sercxo_objekto_uuid(uuid)
	if objekto:
		sxangxi_instance(objekto, on_data['universoObjektoEventoj']['objekto'])


# подписка на действие в кубе нахождения
func subscribtion_kubo():
	var q = QueryObject.new()
	subscription_id = Net.get_current_query_id()
#	Net.send_json(q.test_json(subscription_id))
	Net.send_json(q.kubo_json(subscription_id))


#func set_way_point(position,dock):
#	#останавливаем таймер
#	$timer.stop()
#	$ship.set_way_point(position,dock)
#	$way_point.set_way_point(position)
#	get_node("ship").add_itinero('',position.x, position.y, position.z,-1)
#	#передаём текущие координаты
#	$ship.vojkomenco()#начинаем движение
#	#запускаем таймер
#	$timer.start()

func has_signal_custom(node, sgnl) ->bool:
	if node == null:
		return false
	for i in node.get_signal_list():
		if i.name == sgnl:
			return true
	return false


# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_http_tasko_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	var simpled_data = parsed_resp['data']['redaktuUniversoTaskoj']['universoTaskoj']
	# получаем uuid задачи и помещаем в itinero
	Global.itineroj[0]['uuid_tasko']=simpled_data['uuid']


# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_http_posedanto_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
# warning-ignore:unused_variable
	var parsed_resp = parse_json(resp)
	pass


# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_http_finado_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
# warning-ignore:unused_variable
	var parsed_resp = parse_json(resp)
	pass # Replace with function body.


# warning-ignore:unused_argument
# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _on_http_taskoj_request_completed(result, response_code, headers, body):
	var resp = body.get_string_from_utf8()
	var parsed_resp = parse_json(resp)
	var simpled_data = parsed_resp['data']['redaktuKreiUniversoTaskojPosedanto']['universoTaskoj']
	# получаем список задач и помещаем в itinero
	var i = 0
	for tasko in simpled_data:
		if len(Global.itineroj)>i:
			Global.itineroj[i]['uuid_tasko']=tasko['uuid']
		i += 1


const sxipo = preload("res://blokoj/kosmosxipoj/scenoj/sxipo_fremdulo.tscn")
const sxipo_modulo = preload("res://blokoj/kosmosxipoj/skriptoj/moduloj/sxipo.gd")
const base_ship = preload("res://blokoj/kosmosxipoj/scenoj/base_ship.tscn")
const espero = preload("res://blokoj/kosmostacioj/espero/espero_ekster.tscn")
const asteroido = preload("res://blokoj/asteroidoj/asteroido.tscn")
const ast_1 = preload("res://blokoj/asteroidoj/ast_1.tscn")
const ast_1_cr = preload("res://blokoj/asteroidoj/ast_1_cr.tscn")
const ast_shards_0 = preload("res://blokoj/asteroidoj/ast_shards_0.tscn")
const ast_shards_2 = preload("res://blokoj/asteroidoj/ast_shards_2.tscn")

const AnalizoProjekto = preload("res://blokoj/kosmosxipoj/skriptoj/itineroj.gd")


# функция создания корабля
func create_ship(objecto):
	var ship = null
	
	if (objecto['resurso']['objId'] == 3):# это корабль "Vostok U2" "Базовый космический корабль"
		ship = base_ship.instance()
		var sh = sxipo_modulo.new()
		sh.create_sxipo(ship, objecto)
	if not ship: # проверка, если такого корабля нет в программе
		return null
	ship.translation=Vector3(objecto['koordinatoX'],
		objecto['koordinatoY'], objecto['koordinatoZ'])
	if objecto['rotaciaX']:
		ship.rotation=Vector3(objecto['rotaciaX'],
			objecto['rotaciaY'], objecto['rotaciaZ'])
	ship.visible=true
	ship.uuid=objecto['uuid']
	ship.add_to_group('create')
	$camera.set_privot(ship)
	return ship


# добавляем объект в космос с настройкой местоположения
func add_objekto(objekto,item):
	objekto.translation=Vector3(item['koordinatoX'],
		item['koordinatoY'], item['koordinatoZ'])
	objekto.uuid = item['uuid']
	objekto.objekto = item.duplicate(true)
	objekto.rotation=Vector3(item['rotaciaX'],
		item['rotaciaY'], item['rotaciaZ'])
	add_child(objekto)
	objekto.add_to_group('create')


# функция анализа и добавления объекта в космос на основе пришедшей инфы с сервера
func analizo_item(item):
	if item['resurso']['objId'] == 1:#объект станция Espero
		var state = espero.instance()
		add_objekto(state,item)
		state.add_to_group('state')
	elif (item['resurso']['tipo']['objId'] == 2)and(item['koordinatoX']):# тип - корабль
		var s = sxipo.instance()
		var sh = sxipo_modulo.new()
		sh.create_sxipo(s, item)
		add_objekto(s,item)
		# добавляем маршрут движения
		if item['projekto']:
			var projektoj = item['projekto']['edges']
			var analizo = AnalizoProjekto.new()
			analizo.analizo_projekto(projektoj,s,s)
		s.add_to_group('enemies')
	elif (item['resurso']['objId'] == 7)and(item['koordinatoX']):# тип - астероид
#		подгружаем в зависимости от состояния модификации астероида грузим картинку
		var ast = asteroido.instance()
		if item['stato']['objId']==1:
			ast.add_child(ast_1.instance())
		elif item['stato']['objId']==2:
			ast.add_child(ast_1_cr.instance())
		elif item['stato']['objId']==3:
			ast.add_child(ast_shards_0.instance())
		else:
#			if item['stato']['objId']==4:
			ast.add_child(ast_shards_2.instance())
		add_objekto(ast,item)
		ast.add_to_group('asteroidoj')


# изменить изображение
func sxangxi_instance(objekto, item):
	objekto.integreco = item['integreco']
#	if item['resurso']['objId'] == 1:#объект станция Espero
#		var state = espero.instance()
#		add_objekto(state,item)
#		state.add_to_group('state')
#	elif (item['resurso']['tipo']['objId'] == 2)and(item['koordinatoX']):# тип - корабль
#		var s = sxipo.instance()
#		var sh = sxipo_modulo.new()
#		sh.create_sxipo(s, item)
#		add_objekto(s,item)
#		# добавляем маршрут движения
#		if item['projekto']:
#			var projektoj = item['projekto']['edges']
#			var analizo = AnalizoProjekto.new()
#			analizo.analizo_projekto(projektoj,s,s)
#		s.add_to_group('enemies')
#	el
	if (item['resurso']['objId'] == 7)and(item['koordinatoX']):# тип - астероид
#		 проверить, нужно ли менять визауализацию объекта
		if objekto.objekto['stato']['objId'] == item['stato']['objId']:
#			print('оставляем визуализацию как есть')
			return
#		else:
#			print('меняем визуализацию объекта')
		# сохраняем предыдущее состояние
		var old_ast 
		for ch in objekto.get_children():
			if ch is Spatial:
#				print('==name=',ch.name)
				old_ast = ch
				break
		old_ast.queue_free()
#		подгружаем в зависимости от состояния модификации астероида грузим картинку
		if item['stato']['objId']==1:
			objekto.add_child(ast_1.instance())
		elif item['stato']['objId']==2:
			objekto.add_child(ast_1_cr.instance())
		elif item['stato']['objId']==3:
			objekto.add_child(ast_shards_0.instance())
		else:
#			if item['stato']['objId']==4:
			objekto.add_child(ast_shards_2.instance())
		objekto.objekto['stato']['objId'] = item['stato']['objId']


func _on_space_load_objektoj():
	Title.get_node("CanvasLayer/UI/Objektoj/Window").distance_to($ship.translation)
	# и теперь по uuid нужно найти проект и задачу
	var projektoj = Global.direktebla_objekto[Global.realeco-2]['projekto']['edges']
	var analizo = AnalizoProjekto.new()
	analizo.analizo_projekto(projektoj,get_node("ship"),Global.fenestro_itinero)
	# если корабль полетел, то включаем таймер
	if len(Global.fenestro_itinero.itineroj)>0 and (not Global.fenestro_itinero.itinero_pause):
		$timer.start()
	# создаём остальные объекты в космосе
	for item in Global.objektoj:
		analizo_item(item)


func _on_Timer_timeout():
	var q = QueryObject.new()
	# Делаем запрос к бэкэнду
	Global.direktebla_objekto[Global.realeco-2]['koordinatoX'] = $ship.translation.x
	Global.direktebla_objekto[Global.realeco-2]['koordinatoY'] = $ship.translation.y
	Global.direktebla_objekto[Global.realeco-2]['koordinatoZ'] = $ship.translation.z
	Global.direktebla_objekto[Global.realeco-2]['rotationX'] = $ship.rotation.x
	Global.direktebla_objekto[Global.realeco-2]['rotationY'] = $ship.rotation.y
	Global.direktebla_objekto[Global.realeco-2]['rotationZ'] = $ship.rotation.z
	$http_mutate.request(q.URL, Global.backend_headers, true, 2, 
		q.objecto_mutation($ship.uuid, $ship.translation.x, 
			$ship.translation.y, $ship.translation.z,
			$ship.rotation.x, 
			$ship.rotation.y, $ship.rotation.z)
	)


func _on_space_tree_exiting():
	#разрушаем все созданные объекты в этом мире
	for ch in get_children():
		if ch.is_in_group('create'):
			ch.free()
	Global.fenestro_kosmo = null




