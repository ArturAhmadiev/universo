extends KinematicBody

export var Sensitivity_X: float = 0.01
export var Sensitivity_Y: float = 0.01

const ZOOM_MIN = 1
const ZOOM_MAX = 50
const Zoom_Step: float = 1.0
#const MIN_ROT_Y = -1.55 #(89 градусов)
#const MAX_ROT_Y = 0.79 #(45 градусов)

var max_speed =500.0
# текущая скорость
var current_speed =0
var acceleration = 1
var way_point: Vector3 = Vector3() # Координаты точки, в которую летим
var target_dir: Vector3 = Vector3.ZERO #направление на эту точку от текущей позиции корабля
var target_rot = null #положение корабля, которое надо принять, чтобы нацелиться на точку.
var speed_rotation = 1
var middle_mouse_pressed = false
var docking_rotation

var uuid #uuid активного корабля игрока
#var objekto
var integreco # целостность объекта
var potenco # мощность ресурса

# взятые в прицел, массив объектов, взятых в прицел
var pafado_uuid # uuid проекта ведения огня
var pafado_id # id запроса на создание проекта ведения огня
var celilo_id = [] # id переданных запросов на сервер п
var pafado_celo = [] # на какой объект отправили запрос на установку цели
var celilo = [] # список целей
var elektita_celilo = 0 # выбранная цель
var max_celilo = 1 # максимальное количество прицелов
var armiloj = [] # ссылка на оружие корабля


func _ready():
	$CollisionShape.queue_free() # что бы в процессе редактирования не выдавало ошибку есть данная нода
	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('error = ',err)


func _physics_process(delta):
	# если категория задачи равна категории движения объекта, тогда двигаемся
	if !(Global.fenestro_itinero.itineroj.empty() or \
			Global.fenestro_itinero.itinero_pause) and \
			(Global.fenestro_itinero.itineroj.front()['kategorio']==Net.kategorio_movado): #Если цель существует, двигаемся
		if !target_rot:
#			print('target_rot = ',Global.fenestro_itinero.itineroj.front()['transform'].origin)
			target_rot = Quat(transform.looking_at(Global.fenestro_itinero.itineroj.front()['transform'].origin,Vector3.UP).basis) #запоминаем в какое положение надо установить корабль, чтобы нос был к цели. Это в кватернионах. ХЗ что это, но именно так вращать правильнее всего.

		var target_dir = (Global.fenestro_itinero.itineroj.front()['transform'].origin - translation).normalized()
		var distance = translation.distance_to(Global.fenestro_itinero.itineroj.front()['transform'].origin)
		if (len (Global.fenestro_itinero.itineroj)>1) and \
				(Global.fenestro_itinero.itineroj[1]['kategorio']==Net.kategorio_movado):
			# проверяем, если следующая точка является движением
			if distance <1: #Если это не последняя точка, то мы не тормозим, так что завышаем расстояние, чтобы не проскочить эту точку
				dec_route()
				return
			current_speed = lerp(current_speed,max_speed,delta*acceleration)#ускоряемся
			transform.basis = transform.basis.slerp(target_rot,speed_rotation*delta) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
		else:
			if distance > max_speed*delta/acceleration:#Тут сомнительная формула от фонаря, вычисляющая примерно откуда надо начинать тормозить корабль.
				current_speed = lerp(current_speed,max_speed,delta*acceleration)#ускоряемся
				var a = Quat(transform.basis)
				var c = a.slerp(target_rot,speed_rotation*delta) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
				transform.basis = Basis(c)
#				transform.basis = transform.basis.slerp(target_rot,speed_rotation*delta) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
			else: #Если это  последняя точка, то мы тормозим, и задаём минимальное расстояние, чтобы точнее выставить корабль
				current_speed = lerp(current_speed,50,delta*acceleration)#замедляемся
				if Global.fenestro_itinero.itineroj.front()['transform'].basis !=Basis.IDENTITY:
					transform.basis = transform.basis.slerp(Quat(Global.fenestro_itinero.itineroj.front()['transform'].basis),speed_rotation*delta*1.5) #поворачиваем в дефолтное состояние, чтобы сесть
				if distance <0.01:
					if Global.fenestro_itinero.itineroj.front()['transform'].basis !=Basis.IDENTITY:
						transform.basis = Global.fenestro_itinero.itineroj.front()['transform'].basis
					translation = Global.fenestro_itinero.itineroj.front()['transform'].origin
					clear_route()
					return
# warning-ignore:return_value_discarded
		move_and_slide(target_dir*delta*current_speed) #Двигаемся к цели  и тут можно передать transform на сервер
	Title.get_node("CanvasLayer/UI/Objektoj/Window").distance_to(translation)
	# вывод целей на экран
#	if get_node_or_null('Control'):
	# если есть цель или был произведн выстрел и идёт перезарядка, то продолжаем считывать показатели перезарядки
	if (celilo.size()>0) or ($Control/kanonadi/livero.value>0):
		$Control/kanonadi/livero.value = armiloj.front().livero
#		else:
#			$Control/celilo.set_visible(false)
#			$Control/celilo/label.text = ''


func _on_data():
	var i_data_server = 0
	for on_data in Net.data_server:
		celilo_id
		var celilo_index = celilo_id.find(int(on_data['id']))
		if celilo_index>-1:
			# пришло сообщение, по установлению цели
			celilo_id.remove(celilo_index)
			Net.data_server.remove(i_data_server)
		elif int(on_data['id']) == pafado_id:
			# пришло сообщение, по созданию проекта стрельбы
			pafado_uuid = on_data['payload']['data']['redaktuUniversoProjekto']['universoProjekto']['uuid']
			send_celilo(pafado_celo)
			pafado_id = null
			Net.data_server.remove(i_data_server)
		else:
			print('on_data else=', on_data)
#		elif on_data['payload']['data'].get('filteredUniversoObjekto'):
#			pass
#			Net.data_server.remove(i_data_server)
		i_data_server += 1


const QueryObject = preload("queries.gd")


# устанавливаем цель
func set_celilo(celo):
	# если количество максимальных прицеливаний больше взятых в прицел, тогда берём в прицел
	if max_celilo<=len(celilo):
		return false
	if !celo.get('uuid'):
		return false
	celilo.append(celo)
	$Control/celilo.set_visible(true)
	$Control/celilo/label.text = celilo.front().uuid
	if pafado_uuid: # если проект стрельбы есть, сразу отправляем цель
		send_celilo(celo)
	else:
		var q = QueryObject.new()
		pafado_id = Net.get_current_query_id()
		pafado_celo = celo
		Net.send_json(q.pafado_json(
			Global.direktebla_objekto[Global.realeco-2]['uuid'],pafado_id
		))
		# создаём проект ведения огня
		pass


# отправляем на сервер установку цели
func send_celilo(celo):
	var q = QueryObject.new()
	var id = Net.get_current_query_id()
	celilo_id.append(id)
	Net.send_json(q.celilo_json(
		pafado_uuid,
		Global.direktebla_objekto[Global.realeco-2]['uuid'], # uuid объекта управления
		translation.x, #kom_koordX
		translation.y, #kom_koordY
		translation.z, #kom_koordZ
		celo.uuid,
		celo.translation.x, #kom_koordX
		celo.translation.y, #kom_koordY
		celo.translation.z, #kom_koordZ
		id
	))



func rotate_start():# поворачиваем корабль носом по ходу движения
	var front = Transform(Basis.IDENTITY, Vector3(Global.fenestro_itinero.itineroj.front()['koordinatoX'],
		Global.fenestro_itinero.itineroj.front()['koordinatoY'], 
		Global.fenestro_itinero.itineroj.front()['koordinatoZ']))
	target_rot = Quat(transform.looking_at(front.origin,Vector3.UP).basis) #запоминаем в какое положение надо установить корабль, чтобы нос был к цели. Это в кватернионах. ХЗ что это, но именно так вращать правильнее всего.


func dec_route():
	target_rot = null
	Global.fenestro_itinero.malmultigi_unua()
	rotate_start()


func clear_route():
	target_rot = null
	# достигли цели, закрываем проект
	Global.fenestro_itinero.fermi_projekto_tasko()


func _on_kanonadi_pressed():
	if celilo.size()>0:
		if $Control/kanonadi.pressed:
#			постановка цели оружию
			get_node("laser_gun").set_target(celilo.front())
		else:
			get_node("laser_gun").set_target(null)
	else:
		$Control/kanonadi.pressed = false
		get_node("laser_gun").pafado = false


# отправляем выстрел на сервер (передаются объекты в космосе)
# armile - выстреливше оружие
# celo - цель
func pafo_server(armilo, celo):
	var q = QueryObject.new()
	var id = Net.get_current_query_id()
#	Global.fenestro_kosmo.id_net.append(id)
	Global.fenestro_kosmo.id_pafo.append(id) # до создания сервера удаляет сам клиент
	Net.send_json(q.pafo_json(
		pafado_uuid,
		armilo.uuid, # uuid стреляющего
		translation.x, #kom_koordX
		translation.y, #kom_koordY
		translation.z, #kom_koordZ
		celo.uuid,
		celo.translation.x, #kom_koordX
		celo.translation.y, #kom_koordY
		celo.translation.z, #kom_koordZ
		id
	))


# пришла с сервера задача выстрела
func pafo(projekto, tasko):
	print('=== в стрельбу СВОЮ попали КОСЯК!!!')
	pass
