extends Node


# ТОЛЬКО ДЛЯ СЕРВЕРА
# повреждение от выстрела / изменение целостности объекта
func integreco_shanghi(uuid, integreco, id=0):
	if !id:
		id = Net.get_current_query_id()
	return JSON.print({
	'type': 'start',
	'id': '%s' % id,
	'payload':{ 'query': 'mutation ($uuid:UUID, '+
		' $integreco:Int, )'+
		'{ redaktuUniversoObjektoIntegreco (uuid: $uuid,  '+
		' integreco:$integreco) { status '+
		' message universoObjektoj { uuid } } '+
		'}',
		'variables': {"uuid": uuid, "integreco":integreco } }})




