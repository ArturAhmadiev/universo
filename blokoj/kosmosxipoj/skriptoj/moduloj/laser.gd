extends Spatial

var uuid #нужно для идентификации конкретного модуля
var objekto_modulo
var integreco # целостность объекта
var potenco # мощность ресурса

export (float) var beam_length = 1000
export (float) var speed_rotation = 3
export (Color, RGBA) var laser_color :Color = Color(1.0,0.0,0.0,1.0)
export var x_limiter: Vector2=Vector2(0.0,90.0)
export var y_limiter: Vector2=Vector2(-90.0,90.0)
var new_target = null # задание новой цели
#если таргет сняли или поменяли до окончания показа выстрела, то выстрел демонстрируем до конца по старой цели
var target = null # ведём цель и стреляем
var livero = 0 # произошел выстрел на 1 перезарядка
var id_livero = [] # id ответов по произведённым выстрелам, уменьшаем кол-во боезапаса

var tuttempe_pafo = 50 # втечении времени показывать выстрел
var malfruo = 100 # время задержки выстрела/перезарядка
var kvanto_pafajho = -1 # количество снарядов Если -1 то бесконечный боезапас
#сам лазер отправляет на сервер что он выстрелил и по кому
# обрабатывает результат выстрела оружие - уменьшая кол-во боеприпасов у себя в заряде

var sxipo # указатель на корабль, где установлено данное оружие. Нужно для ведения огня


func _ready():
#	target=get_parent().route #это потом удалить, здесь тестовое получение цели.
	$laser.cast_to.z = -beam_length #устанавливаем максимальную дальность лазера.
	$laser/beam/MeshInstance.mesh.material.set("shader_param/laser_color",laser_color)
	$laser/beam/MeshInstance2.mesh.material.set("shader_param/laser_color",laser_color)

	# подключаем сигнал для обработки входящих данных
	var err = Net.connect("input_data", self, "_on_data")
	if err:
		print('error = ',err)


func _on_data():
	var i_data_server = 0
	for on_data in Net.data_server:
#		print('on_data=', on_data)
#		print('on_data[id] =',on_data['id'],' id_projekto_direkt_del=',id_projekto_direkt_del)
		var index = id_livero.find(int(on_data['id']))
#		print('index=',index,' ::: ', typeof(index), " == ", typeof(on_data['id']))
		if index > -1: # находится в списке выстрелов
#			уменьшаем количество снарядов в оружии
			if kvanto_pafajho>0:
				kvanto_pafajho -= 1
			pass
#			var idx_prj = 0 #индекс массива для удаления
#			for prj in Global.direktebla_objekto[Global.realeco-2]['projekto']['edges']:
#				if prj['node']['uuid']==on_data['payload']['data']['redaktuUniversoProjekto']['universoProjekto']['uuid']:
#					Global.direktebla_objekto[Global.realeco-2]['projekto']['edges'].remove(idx_prj)
#				idx_prj += 1
			id_livero.remove(index)
			Net.data_server.remove(i_data_server)
		i_data_server += 1


func rotate_gun(delta):
	var temp = transform.basis #сохраняем старый базис
#	look_at(target.back().origin,Vector3.UP)#смотрим на цель, это сохранится в текущем базисе, из-за этого и куча движений с буферными базисами
	look_at(target.get_global_transform().origin,Vector3.UP)#смотрим на цель, это сохранится в текущем базисе, из-за этого и куча движений с буферными базисами
	var target_rot = transform.basis#сохраняем целевой базис
	transform.basis = temp#восстанавливаем изначальный базис
	$laser.transform.basis = $laser.transform.basis.slerp(target_rot,speed_rotation*delta) # постепенно поворачиваем к цели
#	$laser.rotation_degrees.x = clamp($laser.rotation_degrees.x,x_limiter.x,x_limiter.y)# ограничиваем вращение пушки вниз, чтобы стреляла только в верхнюю полусферу
#	$laser.rotation_degrees.y = clamp($laser.rotation_degrees.y,y_limiter.x,y_limiter.y)# ограничиваем вращение пушки по сторонам, если нужно
	$gun_body.rotation.y = $laser.rotation.y
	$Turret.rotation.y = $laser.rotation.y


func get_uuid(object):
#	print('==name==',object.name)
	if object.get('uuid'):
#		print('=uuid==',object.uuid)
		return object.uuid
	else:
		return get_uuid(object.get_parent())


# проверка, входит ли uuid в данный объект
#	я могу видеть часть от целого, нужно подняться к предку и проверить его uuid
# проверяем по восходящей до космоса
func sercxado_uuid(uuid, objekt):
	if objekt.get('uuid'):
		if uuid == objekt.uuid:
			return true
	if objekt.get_parent().name != 'space':
		return sercxado_uuid(uuid, objekt.get_parent())
	return false


func _physics_process(delta):
	if target:
#		print('цель задана')
		var length_to_end=beam_length
		var can_shoot = false
		rotate_gun(delta)
		if $laser.is_colliding():
			if sercxado_uuid(target.uuid,$laser.get_collider()):
#				print('стреляем = ',uuid)
	#			print($laser.get_collider().name) #тут можно обработать в кого мы попали и вызвать повреждения.
				length_to_end = get_global_transform().origin.distance_to($laser.get_collision_point())
#				print('тут 1 kvanto_pafajho=',kvanto_pafajho,' tuttempe_pafo=',tuttempe_pafo,' livero=',livero)
				if (tuttempe_pafo>livero) and ((kvanto_pafajho==-1)or(kvanto_pafajho>0)):
					can_shoot = true
					$laser/end_point.visible = true
					$laser/beam.visible = true
					if !livero: # только начали стрелять
						livero = 1 # начало стрельбы после всех проверок
				else:
					$laser/end_point.visible = false
					$laser/beam.visible = false
			else:
				$laser/end_point.visible = false
				$laser/beam.visible = false
		else:
			$laser/end_point.visible = false
			$laser/beam.visible = false
		
		if can_shoot:
			$laser/beam.scale.z=length_to_end
			$laser/end_point.translation.z = -length_to_end
			$laser/end_point/Particles.emitting = true
		else:
			$laser/end_point/Particles.emitting = false
			$laser/beam.scale.z=0.1
			$laser/end_point.translation.z = 0
	else: 
		$laser/end_point.visible = false
		$laser/beam.visible = false
	if livero > 1:
		livero += 1
	if livero == 1: # отправляем выстрел на сервер
		#Это должно быть только на управляемом корабле, а не на оружии всех
		if (sxipo)and(sxipo == Global.fenestro_kosmo.get_node('ship')):
			sxipo.pafo_server(self, target)
		# сервер отвечает за повреждения объектов
#		 отправляем на сервер повреждение от выстрела
		if Global.server:
			integreco_shanghi(potenco)
		livero = 2
	if livero == 10: # производим разрушение объекта цели
		pass
	if livero > malfruo:
		livero = 0
		target = new_target # меняем цель, если за время выстрели цель поменяли или сняли


const QueryObject = preload("queries.gd")


# отправляем на сервер повреждение от выстрела
func integreco_shanghi(nombro):
#	print('===target.integreco-potenco=',target.integreco-potenco)
	if Global.server:
		var q = QueryObject.new()
		var id = Net.get_current_query_id()
		Global.fenestro_kosmo.id_net.append(id)
		Net.send_json(q.integreco_shanghi(
			target.uuid,
			-nombro,
			id
		))


func set_target(targeto):
	if target: # при отмене стрельбы или смена цели
		new_target = targeto
	else: # сюда попадаем только при начале первой стрельбы 
		new_target = targeto
		target = targeto


