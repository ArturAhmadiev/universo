extends KinematicBody


# общий скрипт для всех чужих кораблей

var uuid = ""
var objekto
var distance = 0 # дистанция до корабля игрока
var integreco = 100 # целостность объекта
var potenco

var acceleration = 1 # для расчета движения корабля
var target_rot #положение корабля, которое надо принять, чтобы нацелиться на точку.
var speed_rotation =1

# максимальная скорость корабля
var max_speed =500.0
# текущая скорость
var current_speed =0


# uuid проекта маршрута
var projekto_itineroj_uuid
#маршрут движения корабля
var itineroj = [] 
# uuid - uuid задачи
# front - transform координат цели движения (лучше высчитать один раз при добавлении)
# координаты цели полёта
#			'koordinatoX':
#			'koordinatoY':
#			'koordinatoZ':
# pozicio - в какой позиции должна находится задача
var itinero_pause = false # корабль стоит на паузе


# взятые в прицел, массив объектов, взятых в прицел
var pafado_uuid # uuid проекта ведения огня
var pafado_id # id запроса на создание проекта ведения огня
var celilo_id = [] # id переданных запросов на сервер п
var pafado_celo = [] # на какой объект отправили запрос на установку цели
var celilo = [] # список целей
var elektita_celilo = 0 # выбранная цель
var max_celilo = 1 # максимальное количество прицелов
var armiloj = [] # ссылка на оружие корабля


func _ready():
	$CollisionShape.queue_free()


func _physics_process(delta):
	if !(itineroj.empty() or itinero_pause): #Если цель существует, двигаемся
#		print('летит объект = ',uuid)
		var front = Transform(Basis.IDENTITY,Vector3(itineroj.front()['koordinatoX'],
			itineroj.front()['koordinatoY'], itineroj.front()['koordinatoZ']))
		var target_dir = (front.origin - translation).normalized()
		var distance_target = translation.distance_to(front.origin)
		if len (itineroj)>1:
			if distance_target <1: #Если это не последняя точка, то мы не тормозим, так что завышаем расстояние, чтобы не проскочить эту точку
				dec_route()
				return
			current_speed = lerp(current_speed,max_speed,delta*acceleration)#ускоряемся
			transform.basis = transform.basis.slerp(target_rot,speed_rotation*delta) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
		else:
			if distance_target > max_speed*delta/acceleration:#Тут сомнительная формула от фонаря, вычисляющая примерно откуда надо начинать тормозить корабль.
				current_speed = lerp(current_speed,max_speed,delta*acceleration)#ускоряемся
				var a = Quat(transform.basis)
				var c = a.slerp(target_rot,speed_rotation*delta) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
				transform.basis = Basis(c)
#				transform.basis = transform.basis.slerp(target_rot,speed_rotation*delta) #потихоньку поворачиваем корабль на цель. Взято у Сканера из урока про зомбей. Quat(transform.basis) - текущий поворот корабля
			else: #Если это  последняя точка, то мы тормозим, и задаём минимальное расстояние, чтобы точнее выставить корабль
				current_speed = lerp(current_speed,50,delta*acceleration)#замедляемся
				if front.basis !=Basis.IDENTITY:
					transform.basis = transform.basis.slerp(Quat(front.basis),speed_rotation*delta*1.5) #поворачиваем в дефолтное состояние, чтобы сесть
				if distance_target <0.01:
					if front.basis !=Basis.IDENTITY:
						transform.basis = front.basis
					translation = front.origin
					route_gone()
					return
#		print(distance_target,": ",current_speed)
#		print(len(itineroj))
#		print('двигаемся = ',target_dir*delta*current_speed)
# warning-ignore:return_value_discarded
		move_and_slide(target_dir*delta*current_speed) #Двигаемся к цели  и тут можно передать transform на сервер


func rotate_start():# поворачиваем корабль носом по ходу движения
	var front = Transform(Basis.IDENTITY, Vector3(itineroj.front()['koordinatoX'],
		itineroj.front()['koordinatoY'], itineroj.front()['koordinatoZ']))
	target_rot = Quat(transform.looking_at(front.origin,Vector3.UP).basis) #запоминаем в какое положение надо установить корабль, чтобы нос был к цели. Это в кватернионах. ХЗ что это, но именно так вращать правильнее всего.


func set_route(route_array):
	if !route_array.empty():
		itineroj.clear() #Просто приравнивать нельзя, так как изменяется адрес и если где-то на него кто-то ссылался в других сценах, то он теряет этот адрес Так что очищаем и копируем.
		for i in route_array:
			itineroj.push_back(i)
		rotate_start()


func dec_route():
	itineroj.pop_front()
	set_route(itineroj.duplicate())


func route_gone():
	itineroj.clear()

func add_route(route_array):
	for i in route_array:
		itineroj.push_back(i)
	set_route(itineroj.duplicate())


# добавить точку маршрута 
func add_itinero(uuid_tasko, uuid_celo, nomo, koordinatoX, 
	koordinatoY, koordinatoZ, transform, distance_target, kategorio, 
	pozicio):
	itineroj.append({
		'uuid_tasko':uuid_tasko,
		'uuid_celo':uuid_celo,
		'nomo':nomo,
		'koordinatoX':koordinatoX,
		'koordinatoY':koordinatoY,
		'koordinatoZ':koordinatoZ,
		'transform': transform,
		'distance':distance_target,
		'kategorio':kategorio,
		'pozicio':pozicio
	})


# очищаем маршрут
func malplenigi_itinero():
	route_gone()

# редактировать маршрут
func sxangxi_itinero(projekto, tasko):
	var nomo = ''
#	if (tasko['objekto']) and (uuid != tasko['objekto']['uuid']):
	if uuid != tasko['objekto']['uuid']:
		nomo = tasko['objekto']['nomo']['enhavo']
	var vector = Vector3(tasko['finKoordinatoX'],
		tasko['finKoordinatoY'], 
		tasko['finKoordinatoZ'])
	if projekto['uuid']==projekto_itineroj_uuid: # изменение по задаче в текущем проекте
		var new_tasko = true # признак необходимости новой задачи
		var pos = 0 # номер позиции в списке задач
		for it in itineroj:# проходим по всем задачам
			if tasko['uuid'] == it['uuid']: # нашли соответствующую задачу
				new_tasko = false
				if tasko['statuso']['objId']==Net.statuso_nova: # новая -  в очередь выполнения 
					it['koordinatoX'] = tasko['finKoordinatoX']
					it['koordinatoY'] = tasko['finKoordinatoY']
					it['koordinatoZ'] = tasko['finKoordinatoZ']
				elif tasko['statuso']['objId']==Net.statuso_laboranta: # в работе
					# задача должна быть первой в списке
					if pos: # если не первая
						itineroj.remove(pos)
						itineroj.push_front({
							'uuid':tasko['uuid'],
							'koordinatoX':tasko['finKoordinatoX'],
							'koordinatoY':tasko['finKoordinatoY'],
							'koordinatoZ':tasko['finKoordinatoZ'],
							'uuid_celo':tasko['objekto']['uuid'],
							'nomo': nomo,
							'pozicio': tasko['pozicio'],
							'kategorio': tasko['kategorio']['edges'].front()['node']['objId'],
							'transform': Transform(Basis.IDENTITY, vector),
							'distance': translation.distance_to(vector)
						})
					else:
						it['koordinatoX'] = tasko['finKoordinatoX']
						it['koordinatoY'] = tasko['finKoordinatoY']
						it['koordinatoZ'] = tasko['finKoordinatoZ']
					# отправляем корабль на уточнённые координаты, а точнее поворачиваем
					rotate_start()
					itinero_pause = false
				elif tasko['statuso']['objId']==Net.statuso_fermo: # закрыта
					#удаляем из списка
					itineroj.remove(pos)
				elif tasko['statuso']['objId']==Net.status_pauzo: # приостановлена
					itinero_pause = true
			pos += 1
		if new_tasko: # добавляем новую задачу в проект
			if tasko['statuso']['objId']==Net.statuso_nova: # новая -  в очередь выполнения 
				# нужно выстроить по очерёдности
				pass
				pos = 0
				var pozicio = false
				for it in itineroj:# проходим по всем задачам и находим нужную позицию
					if it['pozicio']>tasko['pozicio']: # вставляем перед данной записью
						itineroj.insert(pos, {
							'uuid':tasko['uuid'],
							'koordinatoX': tasko['finKoordinatoX'],
							'koordinatoY': tasko['finKoordinatoY'],
							'koordinatoZ': tasko['finKoordinatoZ'],
							'pozicio': tasko['pozicio'],
							'uuid_celo':tasko['objekto']['uuid'],
							'nomo': nomo,
							'kategorio': tasko['kategorio']['edges'].front()['node']['objId'],
							'transform': Transform(Basis.IDENTITY, vector),
							'distance': translation.distance_to(vector)
						})
						pozicio = true
						break
					pos += 1
				if !pozicio: # позиция не найдены, добавляем в конце
					itineroj.push_pop({
						'uuid':tasko['uuid'],
						'koordinatoX': tasko['finKoordinatoX'],
						'koordinatoY': tasko['finKoordinatoY'],
						'koordinatoZ': tasko['finKoordinatoZ'],
						'pozicio': tasko['pozicio'],
						'uuid_celo':tasko['objekto']['uuid'],
						'nomo': nomo,
						'kategorio': tasko['kategorio']['edges'].front()['node']['objId'],
						'transform': Transform(Basis.IDENTITY, vector),
						'distance': translation.distance_to(vector)
					})
			elif tasko['statuso']['objId']==Net.statuso_laboranta: # в работе
					# задача должна быть первой в списке
				itineroj.push_front({
					'uuid':tasko['uuid'],
					'koordinatoX':tasko['finKoordinatoX'],
					'koordinatoY':tasko['finKoordinatoY'],
					'koordinatoZ':tasko['finKoordinatoZ'],
					'pozicio': tasko['pozicio'],
					'uuid_celo':tasko['objekto']['uuid'],
					'nomo': nomo,
					'kategorio': tasko['kategorio']['edges'].front()['node']['objId'],
					'transform': Transform(Basis.IDENTITY, vector),
					'distance': translation.distance_to(vector)
				})
					# отправляем корабль на уточнённые координаты, а точнее поворачиваем
				rotate_start()
				itinero_pause = false
			elif tasko['statuso']['objId']==Net.status_pauzo: # приостановлена
				itineroj.push_front({
					'uuid':tasko['uuid'],
					'koordinatoX': tasko['finKoordinatoX'],
					'koordinatoY': tasko['finKoordinatoY'],
					'koordinatoZ': tasko['finKoordinatoZ'],
					'pozicio': tasko['pozicio'],
					'uuid_celo':tasko['objekto']['uuid'],
					'nomo': nomo,
					'kategorio': tasko['kategorio']['edges'].front()['node']['objId'],
					'transform': Transform(Basis.IDENTITY, vector),
					'distance': translation.distance_to(vector)
				})
				itinero_pause = true
	else: # первая запись нового проекта
		route_gone()
		projekto_itineroj_uuid = projekto['uuid']
		itineroj.push_back({
			'uuid':tasko['uuid'],
			'koordinatoX': tasko['finKoordinatoX'],
			'koordinatoY': tasko['finKoordinatoY'],
			'koordinatoZ': tasko['finKoordinatoZ'],
			'pozicio': tasko['pozicio'],
			'uuid_celo':tasko['objekto']['uuid'],
			'nomo': nomo,
			'kategorio': tasko['kategorio']['edges'].front()['node']['objId'],
			'transform': Transform(Basis.IDENTITY, vector),
			'distance': translation.distance_to(vector)
		})
		if tasko['statuso']['objId']==Net.statuso_laboranta: # в работе
			rotate_start()
			itinero_pause = false
		else:
			itinero_pause = true


# пришла с сервера задача выстрела
func pafo(projekto, tasko):
#	=== в стрельбу попали 
	# если стрельба
#	циклом пробегаемся по armiloj
	if tasko['kategorio']['edges'].front()['node']['objId']==Net.tasko_kategorio_pafo:
#		нажодим оружие и передаём ему цель стрельбы
		for armilo in armiloj:
			# ищем, какое именно оружие стреляло
			if armilo.uuid == tasko['posedanto']['edges'].front()['node']['posedantoObjekto']['uuid']:
				# находим по uuid цель в списке объектов космоса
				armilo.set_target(Global.fenestro_kosmo.sercxo_objekto_uuid(tasko['objekto']['uuid']))
#				сразу сбрасываем цель стрельбы в null - тогда будет только один выстрел
				armilo.set_target(null)
	elif tasko['kategorio']['edges'].front()['node']['objId']==Net.tasko_kategorio_celilo:
		print('=== попали в задачу прицеливания projekto=',projekto)
	else:
		print('=== попали не в стрельбу projekto=',projekto)
		print(' ===tasko=',tasko)
	


